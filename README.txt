This is an experimental fork of Crew Chief. 

I advise you to go to the original Crew Chief for now: https://gitlab.com/mr_belowski/CrewChiefV4

The aim is to provide a mininal Crew Chief making it easier to configure and to provide additionnals languages.

There will be no Speech commands, only bases informations with the goal of limiting the number of sound file to provide for a language.

It is centered on rfactor 2 but we will try to keep it generic to every game. We will use international unity, no anglo-saxon.

An additional indirect goal is though this project to help find bugs and eventually propose evolution to the main project.

---------------------------------------------
